package com.puzzlegrama.zusanyapp.ui.theme.basket

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.puzzlegrama.zusanyapp.data.ProductViewModel
import com.puzzlegrama.zusanyapp.data.model.ProductModel


@Composable
fun BasketScreen(viewModel: ProductViewModel) {
    val basket = viewModel.requestCart()
    LazyColumn {
        basket.forEach { (productModel, value) ->
            item {
                productModel?.let { it1 -> BasketCell(it1, value) }
            }
        }
    }
}

@Composable
fun BasketCell(productModel: ProductModel, value: Int) {
    Row {
        Image(
            painter = rememberImagePainter(productModel.images?.get(0)?.src),
            contentDescription = "Contact profile picture",
            modifier = Modifier
                .size(40.dp)
                .clip(CircleShape)
        )

        // Add a horizontal space between the image and the column
        Spacer(modifier = Modifier.width(8.dp))

        Column {
            productModel.name?.let { Text(text = it) }
            // Add a vertical space between the author and message texts
            Spacer(modifier = Modifier.height(4.dp))
            productModel.price?.let { Text(text = it) }
        }
        Spacer(modifier = Modifier.width(8.dp))
        Text(text = value.toString())
    }
}