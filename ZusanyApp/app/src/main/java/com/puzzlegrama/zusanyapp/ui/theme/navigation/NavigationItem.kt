package com.puzzlegrama.zusanyapp.ui.theme.navigation

import com.puzzlegrama.zusanyapp.R

sealed class NavigationItem(var route: String, var icon: Int, var title: String) {
    object ProductList : NavigationItem("productList", R.drawable.ic_product_icon, "ProductList")
    object Basket : NavigationItem("basket", R.drawable.ic_baseline_shopping_basket_24, "Basket")
    object ProductDetail : NavigationItem("productDetail", 0, "productDetail")
    object Game : NavigationItem("game", R.drawable.ic_baseline_videogame_asset_24, "Game")
}