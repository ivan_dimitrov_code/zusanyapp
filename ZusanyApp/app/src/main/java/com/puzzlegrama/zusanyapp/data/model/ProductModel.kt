package com.puzzlegrama.zusanyapp.data.model

import androidx.annotation.Keep

@Keep
class ProductModel {
    var id: String = "0"
    var name: String? = null
    var price: String? = null
    var images: List<ProductImageData>? = null
}