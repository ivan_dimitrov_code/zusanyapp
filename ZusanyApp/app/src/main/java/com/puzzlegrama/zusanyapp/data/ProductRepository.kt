package com.puzzlegrama.zusanyapp.data

import com.puzzlegrama.zusanyapp.api.ApiAdapter
import com.puzzlegrama.zusanyapp.data.model.ProductModel
import kotlinx.coroutines.DelicateCoroutinesApi
import javax.inject.Singleton

sealed class Result<out R> {
    data class Success<out T>(val data: T) : Result<T>()
    data class Error(val exception: Exception) : Result<Nothing>()
}

@Singleton
class ProductRepository public constructor() {
    private val cachedProductList = mutableListOf<ProductModel>()
    private val cart = mutableMapOf<ProductModel?, Int>()

    @OptIn(DelicateCoroutinesApi::class)
    suspend fun requestProductList(): Result<List<ProductModel>?> {
        if (cachedProductList.isNotEmpty()) {
            return Result.Success(cachedProductList)
        }
        return try {
            val response = ApiAdapter.apiClient.getProductList()
            if (response.isSuccessful && response.body() != null) {
                val data = response.body()
                cachedProductList.clear()
                data?.let { cachedProductList.addAll(it) }
                Result.Success(data)
            } else {
                Result.Error(java.lang.Exception("Error while requesting products"))
            }
        } catch (e: Exception) {
            Result.Error(java.lang.Exception(e))
        }
    }

    fun getProductWithID(id: String): ProductModel? {
        return cachedProductList.find {
            it.id == id
        }
    }

    fun addToCart(productId: String) {
        val product = cachedProductList.find {
            it.id == productId
        }

        if (cart.containsKey(product)) {
            cart.put(product, (cart.get(product)!! + 1))
        } else {
            cart[product] = 1
        }
    }

    fun getCart() = cart
}
