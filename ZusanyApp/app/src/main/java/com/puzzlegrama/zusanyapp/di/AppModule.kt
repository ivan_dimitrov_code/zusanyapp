package com.puzzlegrama.zusanyapp.di

import com.puzzlegrama.zusanyapp.data.ProductRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    fun provideProductRepository(): ProductRepository {
        return ProductRepository()
    }
}