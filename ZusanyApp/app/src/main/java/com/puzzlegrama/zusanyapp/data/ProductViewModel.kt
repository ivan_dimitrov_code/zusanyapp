package com.puzzlegrama.zusanyapp.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.puzzlegrama.zusanyapp.data.model.ProductModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductViewModel @Inject public constructor(
    private val repository: ProductRepository
) :
    ViewModel() {

    private val _productData = MutableLiveData<List<ProductModel>>()

    val productData: LiveData<List<ProductModel>>
        get() = _productData

    @DelicateCoroutinesApi
    fun requestData() {
        GlobalScope.launch(Dispatchers.Main) {
            when (val productList = repository.requestProductList()) {
                is Result.Success -> {
                    _productData.value = productList.data
                }
                is Result.Error -> {
                    //TODO show error screen
                }
            }
        }
    }

    fun addToCart(id: String) {
        repository.addToCart(id)
    }

    fun requestCart() = repository.getCart()

    fun getProductById(id: String) = repository.getProductWithID(id)

}