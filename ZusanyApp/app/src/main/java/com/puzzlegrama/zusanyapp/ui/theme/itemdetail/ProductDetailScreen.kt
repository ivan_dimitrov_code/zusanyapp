package com.puzzlegrama.zusanyapp.ui.theme.itemdetail

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import coil.compose.rememberImagePainter
import com.puzzlegrama.zusanyapp.data.ProductViewModel

@Composable
fun ProductDetailScreen(productId: String?, viewModel: ProductViewModel) {
    val product = productId?.let { viewModel.getProductById(it) }

    Column {
        Text(text = "${product?.name}")
        Text(text = "${product?.price}")
        Image(
            painter = rememberImagePainter(product?.images?.get(0)?.src),
            contentDescription = null,
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .fillMaxWidth()
        )
    }
}