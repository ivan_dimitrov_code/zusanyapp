package com.puzzlegrama.zusanyapp.ui.theme.navigation

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.puzzlegrama.zusanyapp.data.ProductRepository
import com.puzzlegrama.zusanyapp.data.ProductViewModel
import com.puzzlegrama.zusanyapp.ui.theme.basket.BasketScreen
import com.puzzlegrama.zusanyapp.ui.theme.game.GameScreen
import com.puzzlegrama.zusanyapp.ui.theme.itemdetail.ProductDetailScreen
import com.puzzlegrama.zusanyapp.ui.theme.list.ProductScreen

@Composable
fun Navigation(navController: NavHostController) {
    val viewModel = ProductViewModel(ProductRepository())

    NavHost(navController, startDestination = NavigationItem.ProductList.route) {
        composable(NavigationItem.ProductList.route) {
            ProductScreen(viewModel, navController)
        }
        composable(NavigationItem.Basket.route) {
            BasketScreen(viewModel)
        }
        composable("${NavigationItem.ProductDetail.route}/{productID}") {
            ProductDetailScreen(it.arguments?.getString("productID"), viewModel)
        }
        composable(NavigationItem.Game.route) {
            GameScreen()
        }
    }
}
