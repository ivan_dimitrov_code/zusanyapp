package com.puzzlegrama.zusanyapp.ui.theme.list

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import coil.compose.rememberImagePainter
import com.puzzlegrama.zusanyapp.data.ProductViewModel
import com.puzzlegrama.zusanyapp.data.model.ProductModel
import com.puzzlegrama.zusanyapp.ui.theme.navigation.NavigationItem


@Composable
fun ProductScreen(viewModel: ProductViewModel, navController: NavHostController) {
    val productList: List<ProductModel> by viewModel.productData.observeAsState(emptyList())
    viewModel.requestData()
    ProductList(productList, navController, viewModel)
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ProductList(
    productList: List<ProductModel>,
    navController: NavHostController,
    viewModel: ProductViewModel
) {
    LazyVerticalGrid(cells = GridCells.Fixed(2)) {
        items(productList) { product ->
            ProductCell(product, navController, viewModel)
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ProductCell(
    product: ProductModel,
    navController: NavHostController,
    viewModel: ProductViewModel
) {
    Card(
        elevation = 4.dp, modifier = Modifier
            .padding(10.dp)
            .height(250.dp),
        onClick = {
            showDetailsScreen(navController, product.id)
        }
    ) {
        Spacer(modifier = Modifier.size(10.dp))
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(text = product.name.toString())
            Text(text = product.price.toString())
            Button(onClick = {
                viewModel.addToCart(product.id)
            }) {
                Text("cart")
            }
            Image(
                painter = rememberImagePainter(product.images?.get(0)?.src),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .fillMaxWidth()
            )

        }
        Spacer(modifier = Modifier.size(10.dp))
    }
}

fun showDetailsScreen(navController: NavHostController, productId: String) {
    navController.navigate("${NavigationItem.ProductDetail.route}/$productId")
}