package com.puzzlegrama.zusanyapp.api

import com.puzzlegrama.zusanyapp.data.model.ProductModel
import retrofit2.Response
import retrofit2.http.GET

interface ApiClient {
    @GET("/wp-json/wc/v2/products?consumer_key=ck_66ec9a155c3dfe3e430bd8972447d12dc7dee655&consumer_secret=cs_5f17ae5bea86221c175416ae227db653a537b6da")
    suspend fun getProductList(): Response<List<ProductModel>>
}