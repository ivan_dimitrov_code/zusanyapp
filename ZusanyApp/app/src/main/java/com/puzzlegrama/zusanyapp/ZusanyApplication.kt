package com.puzzlegrama.zusanyapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ZusanyApplication : Application() {
}